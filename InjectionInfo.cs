﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NextFood
{
    public class InjectionInfo
    {
        public Type OriginType;
        public Type DestinationType;
        public string FunctionName;
        public Func<Type, string, MethodInfo> customMethodSearch;

        public InjectionInfo(Type originType, Type destinationType, string functionName)
        {
            OriginType = originType;
            DestinationType = destinationType;
            this.FunctionName = functionName;
        }

        public InjectionInfo(Type originType, Type destinationType, string functionName,Func<Type,string,MethodInfo> customMethodSearch) : this(originType,destinationType,functionName)
        {
            this.customMethodSearch = customMethodSearch;
        }

    }
}
