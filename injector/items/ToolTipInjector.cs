﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Tooltip;
using Harmony;
using NextFood.Components;
using NextFood.models;

namespace NextFood.injector.items
{
    public class ToolTipInjector
    {

        public static System.Reflection.MethodBase TargetMethod()
        {
            return typeof(TooltipExtensions).GetMethods(BindingFlags.Static | BindingFlags.NonPublic)
                   .FirstOrDefault(m => m.Name.Equals("GetTooltipBSON") &&  m.GetParameters().Length == 4);
        }

        //Function called before Bson tooltip
        public static void Prefix(ref object obj, Player player, IEnumerable<Type> excludes, IEnumerable<Type> includes)
        {
            object foodItem = null;

            if (obj is CraftingElement)
            {
                var crafting = obj as CraftingElement;
                if (crafting.Item is FoodItem)
                {
                    foodItem = crafting.Item;
                }
            }
            else if (obj is FoodItem)
            {
                foodItem = obj;
            }
           
            if(foodItem == null)
                return;
            


    

            try
            {
                var newObj = Activator.CreateInstance(foodItem.GetType()) as FoodItem;

                if (newObj == null)
                {
                    NextFood.printMessage($"Failed to instantiate FoodItem");
                    return;
                }

                obj = newObj;
                ToolTypeComponent.cachedToolTypeInfo.Add(newObj, new ToolTypeInfo(player));
            }
            catch (Exception e)
            {
                NextFood.printMessage($"Something goes wrong during inject tooltype for foodItem {e.Message}");
            }


        }

    }
}
