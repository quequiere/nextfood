﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Eco.Core.Utils;
using Eco.Gameplay.Economy;
using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Shared.Utils;
using Harmony;
using NextFood.Components;
using Exception = System.Exception;

namespace NextFood.injector
{
    public class PlayerGlobalStomach
    {
        public static System.Reflection.MethodBase TargetMethod()
        {
            return typeof(Stomach).GetMethod("get_FoodStatus", BindingFlags.Instance | BindingFlags.Public);
        }


        public static void Postfix(Stomach __instance, ref string __result)
        {

            try
            {
                var tradesField = typeof(EconomyTracker)
                        .GetFields(BindingFlags.Instance | BindingFlags.NonPublic).FirstOrDefault(f => f.Name.Contains("Trades"));
                        
                   var trades = tradesField.GetValue(EconomyTracker.Obj) as ControllerList<TradeTracker>;

                List<Item> foodTradesItems = null;

                try
                {
                    foodTradesItems = trades
                        .Where(t => t.NumberAvailable > 0)
                        .Select(t => Item.Get(t.ItemTypeID))
                        .Where(item => item != null && item is FoodItem)
                        .Where(item =>
                        {
                            try
                            {
                                if (item.DisplayName.NotTranslated != null)
                                    return true;
                            }
                            catch (Exception e)
                            {
                                return false;
                            }

                            return false;
                        }).ToList()
                        .DistinctBy(item => item.DisplayName.NotTranslated)
                        .ToList();
                }
                catch(Exception e)
                {
                    NextFood.printMessage("Error while get trades");
                    NextFood.printMessage(e.Message);
                    NextFood.printMessage(e.StackTrace);
                    NextFood.printMessage(e.GetType().Name);
                }

                if (foodTradesItems == null)
                    return;
        

                   var possibleBuy = foodTradesItems
                       .ToDictionary(it => it, it => FoodCalculatorComponent.getSkillPointsVariation(__instance.Owner.Player, it as FoodItem))
                       .Where(k => k.Value>0)
                       .OrderByDescending(pair => pair.Value).Take(3);

                   string subTittle = possibleBuy.Any()?"This food on the market will increase your skpts:": "There is no interesting food on the market";
                   subTittle = Text.ColorUnity(Color.Yellow.UInt, subTittle);

                   List<string> addedString = new List<string>()
                   {
                       "",
                       "",
                       Text.Size(2f,Text.ColorUnity(Color.Red.UInt,"=== NextFood plugin ===")),
                       subTittle,
                       "",
                   };

                   possibleBuy.ForEach(k =>
                   {
                       string itemValue = Text.Size(1.3f, "+ "+Math.Round(k.Value, 2));
                       itemValue = Text.Bold(itemValue);
                       itemValue = Text.ColorUnity(Color.Green.UInt, itemValue);
                       addedString.Add($"- {k.Key.DisplayName.NotTranslated} for {itemValue} skpts /day");
                   });

                __result += string.Join("\n",addedString);
            }
            catch (Exception e)
            {
                NextFood.printMessage(e.Message);
                NextFood.printMessage(e.StackTrace);
                e.PrettyPrint();
            }
        }
    }
}