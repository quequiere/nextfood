﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NextFood
{
    public class InjectionTool
    {
        public static MethodInfo FindMethod(Type pType, string pName)
        {
            MethodInfo methodInfo = pType.GetMethod(pName, BindingFlags.Instance | BindingFlags.NonPublic);
            if ((object)methodInfo == null)
            {
                MethodInfo method = pType.GetMethod(pName, BindingFlags.Instance | BindingFlags.Public);
                methodInfo = (object)method != null ? method : pType.GetMethod(pName, BindingFlags.Static | BindingFlags.Public);

                if ((object) methodInfo == null)
                {
                    methodInfo = pType.GetMethod(pName, BindingFlags.Static | BindingFlags.NonPublic);
                }
            }
            return methodInfo;
        }
    }
}
