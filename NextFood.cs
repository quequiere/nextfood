﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Asphalt;
using Eco.Core.Plugins.Interfaces;
using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Gameplay.Systems.Tooltip;
using Eco.Shared.Utils;
using Harmony;
using NextFood.Components;
using NextFood.injector;
using NextFood.injector.items;

namespace NextFood
{
    [AsphaltPlugin]
    public class NextFood : IModKitPlugin
    {
        public static string prefix = "NextFood: ";


        public string GetStatus()
        {
            return $"Next food is running ! Cached size: {ToolTypeComponent.cachedToolTypeInfo.Count}";
        }

        public void OnPostEnable()
        {
            printMessage("plugin started !");

            Asphalt.Api.Asphalt.Harmony.Patch(PlayerGlobalStomach.TargetMethod(), null, new HarmonyMethod(typeof(PlayerGlobalStomach), "Postfix"));
            Asphalt.Api.Asphalt.Harmony.Patch(FoodItemOverlay.TargetMethod(), null, new HarmonyMethod(typeof(FoodItemOverlay), "Postfix"));
            Asphalt.Api.Asphalt.Harmony.Patch(ToolTipInjector.TargetMethod(), new HarmonyMethod(typeof(ToolTipInjector), "Prefix"));

            printMessage("plugin loaded !");

        }

        public static void printMessage(string message)
        {
            Console.WriteLine($"{prefix} {message}");
        }
    }
}
